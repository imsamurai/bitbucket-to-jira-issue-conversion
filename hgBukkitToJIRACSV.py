#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by Jonathan Saggau on 2012-11-24.
Copyright (c) 2012 Enharmonic, inc. All rights reserved.
"""

import sys
import os
import getopt
import json
import csv
import codecs
import StringIO

help_message = '''
-i inputfile.json -o outputfile.csv
'''



class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

class ProcessJSON(object):
    inputFile = None
    outputFile = None
    csvLines = u''
    keyMapBukkitToJIRA = {
u'status':u'Status',
u'content':u'Description',
u'title':u'Summary',
u'reported_by.username':u'Reporter',
u'utc_last_updated':u'Updated',
u'created_on':u'Created',
u'priority':u'Priority',
u'metadata.component':u'Labels',
u'metadata.kind':u'Issue Type',
u'responsible.username':u'Assigned'
}
    
    def process(self, inFile, outputFile):
        self.inputFile = inFile;
        self.outputFile = outputFile;
        self._read()
        self._write()

    def _arrayToCSV(self, arrayToConvert, quotechar='\"', quoting=csv.QUOTE_NONNUMERIC):
        fakeFile = StringIO.StringIO()
        writer = csv.writer(fakeFile, delimiter=',', 
                            quotechar=quotechar, quoting=quoting)
        writer.writerow(arrayToConvert);
        outputString = fakeFile.getvalue()
        fakeFile.close()
        return outputString

    def _headerLineOfCSV(self, representativeInDict):
        csvLineArray = self.keyMapBukkitToJIRA.values();
        return self._arrayToCSV(csvLineArray, quotechar='', quoting=csv.QUOTE_NONE)

    def _singleLineOfCSV(self, inDict):
        csvLineArray = [];
        for bitBukkitKey in self.keyMapBukkitToJIRA.keys():
            JIRAKey = self.keyMapBukkitToJIRA[bitBukkitKey]
            try:
                bitBukkitValue = inDict[bitBukkitKey]
                if (bitBukkitKey is u'title' 
                    and (bitBukkitValue is u'' or bitBukkitValue is None)):
                    bitBukkitValue = u'(Empty)'
            except KeyError, exception:
                try:
                    keys = bitBukkitKey.split(u'.')
                    bitBukkitValue = inDict[keys[0]][keys[1]]
                except KeyError, exception:
                    bitBukkitValue = u'(Empty)'
            csvLineArray.append(bitBukkitValue)
        return self._arrayToCSV(csvLineArray)

    def _read(self):
        """Reads and parses the input file"""
        fileHandle = codecs.open(self.inputFile, encoding='utf-8')
        issues = json.load(fileHandle)[u'issues']
        print "issues count = %s" %(len(issues),)
        line = self._headerLineOfCSV(issues[0]) 
        self.csvLines += line + os.linesep
        for eachIssue in issues:
            line = self._singleLineOfCSV(eachIssue)
            self.csvLines += line + os.linesep
        fileHandle.close()

    def _write(self):
        """Writes the output file"""
        try:
            someFile = codecs.open(self.outputFile, 'w', encoding='utf-8')
            someFile.write(self.csvLines)
            someFile.close()
        except Exception as exc:
            print('Error writing %s:%s' %(self.outputFile, exc))
            print
            print

def main(argv=None):
    inFile = None
    outFile = None

    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "ho:i:", ["help", "output=", "input="])
        except getopt.error, msg:
            raise Usage(msg)
    
        # option processing
        for option, value in opts:
            if option in ("-h", "--help"):
                raise Usage(help_message)
            if option in ("-o", "--output"):
                outFile = value
            if option in ("-i", "--input"):
                inFile = value
        processor = ProcessJSON()
        if (inFile is None or outFile is None):
            raise Usage(help_message)
        else:
            processor.process(inFile, outFile)

    except Usage, err:
        print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
        print >> sys.stderr, "\t for help use --help"
        return 2


if __name__ == "__main__":
    sys.exit(main())
